<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php print $docTitle?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <?php print $css?>

         <!-- JQUERY LIB -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
        <!-- JQUERY LIB -->

        <script src="js/vendor/modernizr-2.6.2.min.js"></script>

        <!-- FLEXSLIDER -->
		<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
		<script defer src="js/jquery.flexslider.js"></script>
		<!-- FLEXSLIDER -->

        <!-- fancybox -->
        <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-1.3.4.css" media="screen" />
        <script type="text/javascript" src="fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <!-- fancybox -->

        <link rel="stylesheet" type="text/css" href="../../../mediaelement/mediaelementplayer.css" charset="utf-8">
        <?php print $head?>

    </head>
    <body class="page">
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

		<!-- outer -->
        <div id="outer">
        	<!-- metanavigation -->
            <div id="topbar">
                <div class="contentWrapper clearfix">
                    <?php print $languages; ?>
                    <nav class="menu-top visible-tablet visible-desktop">
                        <?php print $metanavigation; ?>
                    </nav>
                    <?php print $search; ?>
                </div>
            </div>
            <!-- metanavigation -->
            <div class="contentWrapper clearfix container-fluid">
            	<!-- header -->
                <header class="clearfix row-fluid">
                    <div class="span3 visible-tablet visible-desktop">
                       <?php print $title3; ?>
						
						
						
						
						
						
						
                    </div>
                    <div class="<?php print ($title4 ? 'span6' : 'span9') ?>">
                        
                        <h2><?php print $title2; ?></h2>
											
						

                    </div>
                    <?php if ($title4) {?>
                    <div class="span3 visible-tablet visible-desktop">
                        <!--<?php print $title4; ?>-->
								
		
        <script type="text/javascript">
	
            $(document).ready(function()
            {       
                setInterval (rotatore, 500);
                var images = new Array ('1.png','2.png','3.png','4.png');
                var index = 1;
                function rotatore()
                {
                    $('.sfumatore img').fadeOut(200, function()
                    {
                        $(this).attr('src', images[index]);    
                        $(this).fadeIn('slow', function()
                        {
                            if (index == images.length-1)
                            {
                                index = 0;
                            }
                            else
                            {
                                index++;
                            }
                        });
                    });
                }
            });
        </script>        
       <div class="sfumatore">
            <img width="50" height="50" src="2.png" alt="5" />
			
        </div>
    </body>
</html>  	
						
						
						
						
						
						
						
						
						
                 	
					
					
					
					</div>
                    <!--<?php } ?>-->
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
                </header>
                <!-- header -->

                <!-- show-nav-for-iphone -->
                <button type="button"  class="show-nav-for-iphone span12" data-toggle="collapse" data-target="#nav-collapse">MENU DI NAVIGAZIONE</button>
                <!-- show-nav-for-iphone -->

                <!-- breadcrumb -->
                <article class="main-content view-one-column span12">
                    <?php if ($breadcrumbs) { ?>
                        <nav class="breadcrumb">
                            <?php print $breadcrumbs; ?>
                        </nav>
                    <?php } ?>
                    <?php if ($pageTitle) { ?>
                        <div class="box-title">
                             <?php print $pageTitle; ?>
                            <a href="javascript:window.print()" class="print">print this page</a>
                        </div>
                    <?php } ?>
                    <div class="content-box row-fluid ">
                        <?php if ($navigation) { ?>
                            <div id="nav-collapse" class="collapse span3">
                                <nav class="menu-sx span12">
                                    <?php print $navigation; ?>
                                </nav>
                            </div>
                        <?php } ?>
                        <?php print $content; ?>
						
						
						
						
						
				
                    </div>
                    <?php print $afterContent; ?>
                </article>
            </div>
            <!-- content -->

        </div>
        <!-- outer -->

       <footer>
            <div class="contentWrapper clearfix">
                
                    
                </div>
                <div class="box">
                    <div id="nav-collapse-box1">
                        <nav>
						<?php print $logoFooter; ?>
                           
						  <!--<?php print $linkFooter; ?>--> 
						  <table>
<td>
<img src="https://phaidra.cab.unipd.it/static/padova/img/logos/phaidra-logo.png" alt="Phaidra - Collezioni digitali">
</td>
<td>
<form action="https://phaidra.cab.unipd.it/search_object" id="form_quicksearch" name="form_quicksearch" method="get" onsubmit="check_submit(document.getElementById('0_0_field').value,document.getElementById('0_0_value').value);">
<label class="screen-reader" for="0_0_value">Testo da ricercare:</label><input class="search" name="0_0_value" id="0_0_value" value="ananas" type="text">
</td>
<td>
<input value="Vai" class="submit" type="submit">
</td>
<td>
<label class="screen-reader" for="0_0_field">Tipo di ricerca:</label>
<select name="0_0_field" id="0_0_field" size="1" onchange="javascript:check_quicksearch(this.value);">
					
						<option value="-3" selected="selected">Ricerca libera</option>
					
						<option value="1">Identificatore</option>
					
						<option value="2">Titolo</option>
					
						<option value="4">Descrizione</option>
					
						<option value="12">Autore</option>
					
						<option value="-5">Ricerca nei titoli dei libri</option>
</select>
</td>  		
	
				<input name="0_0_choice" id="0_0_choice" value="comp" type="hidden">
				<input name="0_0_lang" value="XX" type="hidden">  
				<input name="search" value="1" type="hidden">
				<input name="quicksearch" value="1" type="hidden">

							 				  
</table> 
               
           
            <div id="info-page" class="visible-desktop">
                <div class="contentWrapper container-fluid">
                    
                </div>
            </div>
        </footer>

        <script src="js/plugins.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        <script src="../../../mediaelement/mediaelement-and-player.js" type="text/javascript"></script>

    </body>
</html>


      
