<?php
/**
 * This file is part of the GLIZY framework.
 * Copyright (c) 2005-2012 Daniele Ugoletti <daniele.ugoletti@glizy.com>
 * 
 * For the full copyright and license information, please view the COPYRIGHT.txt
 * file that was distributed with this source code.
 */

 

 
 
 
 
class org_glizy_Session
{
	function init()
	{
		org_glizy_Session::start();
	}

	
	
		 
       
	

	
	function start()
	{
		if (!org_glizy_Session::isStarted())
		{
			if (!isset($_SESSION)) 
			{
				
				
				/*session_start();*/
				
				
				

			}
			$prefix = org_glizy_Config::get('SESSION_PREFIX');
			$timeout = org_glizy_Config::get('SESSION_TIMEOUT');
			if ( isset( $_SESSION[$prefix.'sessionLastAction'] ) && time() - $_SESSION[$prefix.'sessionLastAction'] > $timeout )
			{
				$_SESSION=array();
			}
			$_SESSION[$prefix.'sessionStarted'] = true;
			$_SESSION[$prefix.'sessionLastAction'] = time();
		}
	}
	
	function stop()
	{
		if (org_glizy_Session::isStarted())
		{
			org_glizy_Session::set('sessionStarted', false);
			session_write_close();
		}
	}

	function destroy()
	{
		if (org_glizy_Session::isStarted())
		{
			session_start();
			$_SESSION=array();
			session_unset();
			session_destroy();
		}
	}
	

	function isStarted()
	{
		$prefix = org_glizy_Config::get('SESSION_PREFIX');
		if ( isset($_GET['draft']) && $_GET['draft'] != '' && isset($_GET['sespre']) && $_GET['sespre'] != '' )
		{
			$prefix = $_GET['sespre'];
			org_glizy_Config::set('SESSION_PREFIX', $prefix);
		}
		if (!isset($_SESSION) || !isset($_SESSION[$prefix.'sessionStarted']) || $_SESSION[$prefix.'sessionStarted']!==true)  return false;
		else return true;
	}

	
	function get($key, $defaultValue=NULL, $readFromParams=false, $writeDefaultValue=false)
	{
		org_glizy_Session::start();
		if (!array_key_exists(org_glizy_Config::get('SESSION_PREFIX').$key, $_SESSION))
		{
			$value = ($readFromParams) ? org_glizy_Request::get($key, $defaultValue) : $defaultValue;
			if ($writeDefaultValue) org_glizy_Session::set($key, $value);
		}
		else
		{
			$value = $_SESSION[org_glizy_Config::get('SESSION_PREFIX').$key];
		}
		return $value; 
	}
	
	function set($key, $value)
	{
		org_glizy_Session::start();
		$_SESSION[org_glizy_Config::get('SESSION_PREFIX').$key] = $value;
	}
	

	function exists($key)
	{
		org_glizy_Session::start();
		return isset($_SESSION[org_glizy_Config::get('SESSION_PREFIX').$key]);
	}
	
	
	function remove($key)
	{
		org_glizy_Session::start();
		$key = org_glizy_Config::get('SESSION_PREFIX').$key;
		if (array_key_exists($key, $_SESSION))
		{
			unset($_SESSION[$key]);
		}
	}
    
    function removeKeysStartingWith($keyPrefix)
    {
		org_glizy_Session::start();
    	$keyPrefix = org_glizy_Config::get('SESSION_PREFIX').$keyPrefix;
        foreach ($_SESSION as $k => $v) {
            if (substr($k, 0, strlen($keyPrefix)) == $keyPrefix) {
		    	unset($_SESSION[$k]);
            }
        }
	}

	function removeAll()
	{
		org_glizy_Session::destroy();
		org_glizy_Session::start();
	}
	
	
	function getAllAsArray()
	{
		org_glizy_Session::start();
		return $_SESSION;
	}

	function setFromArray($values)
	{
		org_glizy_Session::start();
		foreach($values as $k=>$v)
		{
			$_SESSION[org_glizy_Config::get('SESSION_PREFIX').$k] = $v;
		}
	}
	
	function getSessionId()
	{
		return session_id();
	}
	
	
	function dump()
	{
		org_glizy_Session::start();
		var_dump($_SESSION);
	}
	
	function &_getValuesArray($init=false)
	{	
		if (!$init)
		{
			org_glizy_Session::init();
		}
		return $_SESSION;
	}	
}

// shortchut version
class __Session extends org_glizy_Session
{
}


?>